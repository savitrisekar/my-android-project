package com.example.sampel.akun.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sampel.R;
import com.example.sampel.home.BukaMerchantActivity;
import com.example.sampel.akun.EditProfileActivity;


public class ProfileFragment extends Fragment {

    private View viewProfile;
    private ImageView ivLogout;
    private TextView tvUbahAkun;
    private Button btnBukamerchant;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewProfile = inflater.inflate(R.layout.fragment_profile, container, false);

        initView();
        return viewProfile;
    }

    private void initView() {
        ivLogout = viewProfile.findViewById(R.id.iv_logout);
        tvUbahAkun = viewProfile.findViewById(R.id.tv_ubah_akun);
        btnBukamerchant = viewProfile.findViewById(R.id.btn_buka_merchant);

        ivLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Berhasil logout", Toast.LENGTH_SHORT).show();
            }
        });

        tvUbahAkun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(getActivity(), "Berhasil logout", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                startActivity(intent);
            }
        });

        btnBukamerchant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(getActivity(), "Buka merchant", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), BukaMerchantActivity.class);
                startActivity(intent);
            }
        });
    }
}
