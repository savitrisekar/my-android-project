package com.example.sampel.akun.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sampel.R;
import com.example.sampel.akun.EditMerchantActivity;
import com.example.sampel.akun.EditProfileActivity;


public class ProfileMerchantFragment extends Fragment {

    private View viewMerchant;
    private ImageView ivLogout;
    private TextView tvUbahAkun, tvUbahMerchant;

    public ProfileMerchantFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewMerchant = inflater.inflate(R.layout.fragment_profile_merchant, container, false);

        initView();
        return viewMerchant;
    }

    private void initView() {
        ivLogout = viewMerchant.findViewById(R.id.iv_logout);
        tvUbahAkun = viewMerchant.findViewById(R.id.tv_ubah_akun);
        tvUbahMerchant = viewMerchant.findViewById(R.id.tv_ubah_merchant);

        ivLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Berhasil logout", Toast.LENGTH_SHORT).show();
            }
        });

        tvUbahAkun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(getActivity(), "Berhasil logout", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                startActivity(intent);
            }
        });

        tvUbahMerchant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(getActivity(), "Berhasil logout", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), EditMerchantActivity.class);
                startActivity(intent);
            }
        });
    }
}
