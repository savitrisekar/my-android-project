package com.example.sampel.validation;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.sampel.MainActivity;
import com.example.sampel.R;
import com.google.android.material.textfield.TextInputLayout;

public class LoginActivity extends AppCompatActivity {

    private TextInputLayout tilUsername, tilPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        initView();
    }

    private void initView() {
        tilUsername = findViewById(R.id.til_username);
        tilPassword = findViewById(R.id.til_password);
    }

    private boolean validateUsername() {
        String usernameInput = tilUsername.getEditText().getText().toString().trim();
        if (usernameInput.isEmpty()) {
            tilUsername.setError("Field can't be empty");
            return false;
        } else if (usernameInput.length() > 10) {
            tilUsername.setError("Username too long");
            return false;
        } else {
            tilUsername.setError(null);
            return true;
        }
    }

    private boolean validatePassword() {
        String passwordInput = tilPassword.getEditText().getText().toString().trim();
        if (passwordInput.isEmpty()) {
            tilPassword.setError("Field can't be empty");
            return false;
        } else {
            tilPassword.setError(null);
            return true;
        }
    }

    public void onSignIn(View view) {
//        Toast.makeText(this, "Login success", Toast.LENGTH_SHORT).show();
        if (!validateUsername() | !validatePassword()) {
            return;
        }
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    public void onForgot(View view) {
        Toast.makeText(this, "Forgot?", Toast.LENGTH_SHORT).show();
    }

    public void onSignHere(View view) {
        startActivity(new Intent(this, RegisterActivity.class));
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
    }
}
