package com.example.sampel.validation;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.sampel.R;
import com.google.android.material.textfield.TextInputLayout;

public class RegisterActivity extends AppCompatActivity {

    private TextInputLayout tilUsername, tilEmail, tilPassword, tilConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        changeStatusBarColor();
        initView();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }
    }

    private void initView() {
        tilUsername = findViewById(R.id.til_username);
        tilEmail = findViewById(R.id.til_email);
        tilPassword = findViewById(R.id.til_password);
        tilConfirm = findViewById(R.id.til_confirm_password);
    }

    private boolean validateUsername() {
        String usernameInput = tilUsername.getEditText().getText().toString().trim();
        if (usernameInput.isEmpty()) {
            tilUsername.setError("Field can't be empty");
            return false;
        } else if (usernameInput.length() > 10) {
            tilUsername.setError("Username too long");
            return false;
        } else {
            tilUsername.setError(null);
            return true;
        }
    }

    private boolean validateEmail() {
        String emailInput = tilEmail.getEditText().getText().toString().trim();
        if (emailInput.isEmpty()) {
            tilEmail.setError("Field can't be empty");
            return false;
        } else {
            tilEmail.setError(null);
            return true;
        }
    }

    private boolean validatePassword() {
        String passwordInput = tilPassword.getEditText().getText().toString().trim();
        if (passwordInput.isEmpty()) {
            tilPassword.setError("Field can't be empty");
            return false;
        } else {
            tilPassword.setError(null);
            return true;
        }
    }

    private boolean validateConfirmPassword() {
        String confirmInput = tilConfirm.getEditText().getText().toString().trim();
        if (confirmInput.isEmpty()) {
            tilConfirm.setError("Field can't be empty");
            return false;
        } else if (!validatePassword()) {
            tilConfirm.setError("Wrong password!");
            return false;
        } else {
            tilConfirm.setError(null);
            return true;
        }
    }

    public void onSignUp(View view) {
//        Toast.makeText(this, "Sign up success", Toast.LENGTH_SHORT).show();
        if (!validateUsername() | !validateEmail() | !validatePassword() | !validateConfirmPassword()) {
            return;
        }

        String input = "Email: " + tilEmail.getEditText().getText().toString();
        input += "\n";
        input += "Username: " + tilUsername.getEditText().getText().toString();
        input += "\n";
        input += "Password: " + tilPassword.getEditText().getText().toString();
        input += "\n";
        input += "Confirm password: " + tilConfirm.getEditText().getText().toString();

        Toast.makeText(this, input, Toast.LENGTH_SHORT).show();
    }

    public void onBackSignIn(View view) {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
        overridePendingTransition(R.anim.slide_in_left, android.R.anim.slide_out_right);
    }
}
