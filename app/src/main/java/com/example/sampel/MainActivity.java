package com.example.sampel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.example.sampel.custom.CurvedBottomNavigationView;
import com.example.sampel.home.fragment.HomeFragment;
import com.example.sampel.akun.fragment.ProfileFragment;
import com.example.sampel.home.scanner.ScannerActivity;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        CurvedBottomNavigationView curvedNav = findViewById(R.id.custom_navbar);
//        curvedNav.inflateMenu(R.menu.bottom_menu);
//        curvedNav.setSelectedItemId(R.id.menu_home);
//        curvedNav.setOnNavigationItemSelectedListener(navListener);

//        BottomAppBar bar = findViewById(R.id.bottom_appbar);
//        setSupportActionBar(bar);
        BottomNavigationView btmNav = findViewById(R.id.bottom_navigation);
        btmNav.inflateMenu(R.menu.bottom_menu);
        btmNav.setSelectedItemId(R.id.menu_home);
        btmNav.setOnNavigationItemSelectedListener(navListener);

        FloatingActionButton fab = findViewById(R.id.fab_scan);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(MainActivity.this, "click me!", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(MainActivity.this, ScannerActivity.class));
            }
        });

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new HomeFragment()).commit();
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment selectedFragment = null;

            switch (menuItem.getItemId()) {
                case R.id.menu_home:
                    selectedFragment = new HomeFragment();
                    break;
                case R.id.menu_shop:
                    break;
                case R.id.menu_account:
                    selectedFragment = new ProfileFragment();
                    break;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.main_container, selectedFragment).commit();
            return true;
        }
    };
}
