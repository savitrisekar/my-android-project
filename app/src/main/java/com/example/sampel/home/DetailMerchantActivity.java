package com.example.sampel.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.sampel.R;
import com.example.sampel.home.scanner.ScannerActivity;

public class DetailMerchantActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_merchant);

        Toolbar toolbar = findViewById(R.id.toolbar_detile);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void onScanDetile(View view) {
//        Toast.makeText(this, "Scan please!", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(DetailMerchantActivity.this, ScannerActivity.class));
    }
}
