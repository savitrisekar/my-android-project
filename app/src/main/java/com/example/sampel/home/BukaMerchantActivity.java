package com.example.sampel.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.sampel.R;
import com.example.sampel.home.notif.NotifikasiMerchantActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class BukaMerchantActivity extends AppCompatActivity {

    private ImageView ivPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buka_merchant);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        initView();
    }

    private void initView() {
        ivPhoto = findViewById(R.id.iv_photo);
        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(BukaMerchantActivity.this, "Change image", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onBukaMerchant(View view) {
//        Toast.makeText(this, "Buka merchant berhasil", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(BukaMerchantActivity.this, NotifikasiMerchantActivity.class));
    }
}
