package com.example.sampel.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.sampel.R;
import com.example.sampel.home.adapter.MerchantAdapter;

public class ShowMoreActivity extends AppCompatActivity {

    private RecyclerView rvShowMore;
    private MerchantAdapter merchantAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_more);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initView();
        setupRecycler();
        setupAdapter();
    }

    private void initView() {
        rvShowMore = findViewById(R.id.rv_show_more);
    }

    private void setupRecycler() {
        rvShowMore.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ShowMoreActivity.this);
        rvShowMore.setLayoutManager(linearLayoutManager);
    }

    private void setupAdapter() {
        merchantAdapter = new MerchantAdapter();
        rvShowMore.setAdapter(merchantAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.searchview_menu, menu);

        MenuItem seacrh = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(seacrh);
        searchView.setQueryHint("Cari merchant");
        searchView.setIconified(true);
        seacrh(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void seacrh(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
//                showAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }
}
