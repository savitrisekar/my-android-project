package com.example.sampel.home.scanner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.sampel.R;
import com.example.sampel.home.notif.NotifikasiScanActivity;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

public class ScannerActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 42;

    private CaptureManager capture;
    private DecoratedBarcodeView decoratedBarcodeView;
    //    private Button btnKanan, btnKiri;
    private ImageView ivCodeScan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);

        initView();

//        if (savedInstanceState != null) {
//            init(decoratedBarcodeView, getIntent(), savedInstanceState);
//        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        capture = new CaptureManager(this, decoratedBarcodeView);
        capture.initializeFromIntent(getIntent(), savedInstanceState);
        capture.decode();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
//            init(decoratedBarcodeView, getIntent(), null);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    MY_PERMISSIONS_REQUEST_CAMERA);
        }
    }

//    private void init(DecoratedBarcodeView decoratedBarcodeView, Intent intent, Bundle savedInstanceState) {
//        capture  = new CaptureManager(this, decoratedBarcodeView);
//        capture .initializeFromIntent(intent, savedInstanceState);
//        capture .decode();
//    }

    private void initView() {
        ivCodeScan = findViewById(R.id.iv_code_scan);
        decoratedBarcodeView = findViewById(R.id.zxing_barcode_scanner);

        ivCodeScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(ScannerActivity.this, "Yeay code berhasil!", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(ScannerActivity.this, NotifikasiScanActivity.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        capture.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return decoratedBarcodeView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    init(decoratedBarcodeView, getIntent(), null);
                } else {
                    setResult(Activity.RESULT_CANCELED);
                    finish();
                }
            }
        }
    }
}