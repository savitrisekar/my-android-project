package com.example.sampel.home.model;

import com.example.sampel.R;

public class Merchant {

    public static String[] nameMerchant = new String[]{
            "Bakmi Mewah Cak Lontong",
            "Bakmi Mewah Cak Lontong",
            "Bakmi Mewah Cak Lontong",
            "Bakmi Mewah Cak Lontong",
            "Bakmi Mewah Cak Lontong",
            "Bakmi Mewah Cak Lontong",
            "Bakmi Mewah Cak Lontong",
            "Bakmi Mewah Cak Lontong",
            "Bakmi Mewah Cak Lontong"
    };

    public static int[] imgMerchant = new int[]{
            R.drawable.img_merchant,
            R.drawable.header,
            R.drawable.img_merchant,
            R.drawable.header,
            R.drawable.img_merchant,
            R.drawable.img_merchant,
            R.drawable.header,
            R.drawable.img_merchant,
            R.drawable.header
    };

    public static String[] addressMerchant = new String[]{
            "No.29 Rt.30 Kel., Jl. Jenderal Sudirman, Klandasan Ulu, Balikpapan Kota, Kota Balikpapan, Kalimantan Timur, Indonesia",
            "No.29 Rt.30 Kel., Jl. Jenderal Sudirman, Klandasan Ulu, Balikpapan Kota, Kota Balikpapan, Kalimantan Timur, Indonesia",
            "No.29 Rt.30 Kel., Jl. Jenderal Sudirman, Klandasan Ulu, Balikpapan Kota, Kota Balikpapan, Kalimantan Timur, Indonesia",
            "No.29 Rt.30 Kel., Jl. Jenderal Sudirman, Klandasan Ulu, Balikpapan Kota, Kota Balikpapan, Kalimantan Timur, Indonesia",
            "No.29 Rt.30 Kel., Jl. Jenderal Sudirman, Klandasan Ulu, Balikpapan Kota, Kota Balikpapan, Kalimantan Timur, Indonesia",
            "No.29 Rt.30 Kel., Jl. Jenderal Sudirman, Klandasan Ulu, Balikpapan Kota, Kota Balikpapan, Kalimantan Timur, Indonesia",
            "No.29 Rt.30 Kel., Jl. Jenderal Sudirman, Klandasan Ulu, Balikpapan Kota, Kota Balikpapan, Kalimantan Timur, Indonesia",
            "No.29 Rt.30 Kel., Jl. Jenderal Sudirman, Klandasan Ulu, Balikpapan Kota, Kota Balikpapan, Kalimantan Timur, Indonesia",
            "No.29 Rt.30 Kel., Jl. Jenderal Sudirman, Klandasan Ulu, Balikpapan Kota, Kota Balikpapan, Kalimantan Timur, Indonesia"
    };
}
