package com.example.sampel.home.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sampel.R;
import com.example.sampel.home.DetailMerchantActivity;
import com.example.sampel.home.model.Merchant;

public class MerchantAdapter extends RecyclerView.Adapter {

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_merchant, parent, false);
        return new HomeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((HomeViewHolder) holder).bindView(position);
    }

    @Override
    public int getItemCount() {
        return Merchant.nameMerchant.length;
    }

    private class HomeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView name, address;
        private ImageView imgMerchant;

        public HomeViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.tv_item_merchant);
            address = view.findViewById(R.id.tv_item_location);
            imgMerchant = view.findViewById(R.id.iv_item_merchant);
            view.setOnClickListener(this);
        }

        public void bindView(int position) {
            name.setText(Merchant.nameMerchant[position]);
            address.setText(Merchant.addressMerchant[position]);
            imgMerchant.setImageResource(Merchant.imgMerchant[position]);
        }


        @Override
        public void onClick(View view) {
            Context context = view.getContext();
            context.startActivity(new Intent(context, DetailMerchantActivity.class));
//            Toast.makeText(view.getContext(), "Clicked!", Toast.LENGTH_SHORT).show();
//            Log.d("Tag", "click: " + view.toString());
        }
    }
}
