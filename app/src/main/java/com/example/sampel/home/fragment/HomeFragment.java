package com.example.sampel.home.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sampel.R;
import com.example.sampel.home.InfoSaldoActivity;
import com.example.sampel.home.ShowMoreActivity;
import com.example.sampel.home.adapter.MerchantAdapter;

public class HomeFragment extends Fragment {

    private View viewHomeMerchant;
    private RecyclerView rvMerchant;
    private MerchantAdapter homeAdapter;
    private TextView tvShowMore;
    private CardView cv;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewHomeMerchant = inflater.inflate(R.layout.fragment_home, container, false);

        initView();
        setupRecycler();
        setupAdapter();
        return viewHomeMerchant;
    }

    private void initView() {
        rvMerchant = viewHomeMerchant.findViewById(R.id.rv_merchan_home);
        tvShowMore = viewHomeMerchant.findViewById(R.id.tv_lihat_semua);
        cv = viewHomeMerchant.findViewById(R.id.cv_info_poin);

        cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), InfoSaldoActivity.class);
                startActivity(intent);
            }
        });

        tvShowMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ShowMoreActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setupRecycler() {
        rvMerchant.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getActivity());
        rvMerchant.setLayoutManager(linearLayoutManager);
    }

    private void setupAdapter() {
        homeAdapter = new MerchantAdapter();
        rvMerchant.setAdapter(homeAdapter);
    }
}
