package com.example.sampel.home.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;

import com.example.sampel.R;
import com.example.sampel.home.notif.NotifikasiTarikActivity;

public class TarikSaldoFragment extends Fragment {

    private View viewTarikSaldo;
    private CheckBox checkBox;
    private Button btnTarikSaldo;

    public TarikSaldoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewTarikSaldo = inflater.inflate(R.layout.fragment_tarik_saldo, container, false);

        initView();
        return viewTarikSaldo;
    }

    private void initView() {
        checkBox = viewTarikSaldo.findViewById(R.id.cb_tarik);
        btnTarikSaldo = viewTarikSaldo.findViewById(R.id.btn_tarik_saldo);

        btnTarikSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NotifikasiTarikActivity.class);
                startActivity(intent);
            }
        });

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isChecked = ((CheckBox) view).isChecked();
                if (isChecked) {
                    Log.d("TAG", "response >> ");
                }
            }
        });
    }
}
