package com.example.sampel.home.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.sampel.R;
import com.example.sampel.home.notif.NotifikasiSaldoActivity;

public class IsiSaldoFragment extends Fragment {

    private View viewIsiSaldo;
    private Button btnIsiSaldo;

    public IsiSaldoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewIsiSaldo = inflater.inflate(R.layout.fragment_isi_saldo, container, false);

        initView();
        return viewIsiSaldo;
    }

    private void initView() {
        btnIsiSaldo = viewIsiSaldo.findViewById(R.id.btn_isi_saldo);
        btnIsiSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NotifikasiSaldoActivity.class);
                startActivity(intent);
            }
        });
    }
}
